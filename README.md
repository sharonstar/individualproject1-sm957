# Individual Project 1

**Requirements:**
* Website built with ZolaLinks to an external site., Hugo, Gatsby, Next.js or equivalent
* GitLab workflow to build and deploy site on push
* Hosted on Vercel, Netlify, AWS Amplify, AWS S3, or others.

**Author:**   Sharon Ma

**Website link:**

Netlify:  https://sharonstar.netlify.app


Demo Video:  https://youtu.be/hm-zSw92tEI?si=L8Rfx8xKpRVqAps5

# Build/Deploy the Website

- Create the website using zola  `zola init projectname`
- Build your website in the content directory
- After finishing the website, build the website  `zola build`
- Push your code to GitLab and set up the GitLab CI/CD pipeline. Then create the `.gitlab-ci.yml` file provided by Zola to set up the GitLab Runner.
```
stages:
  - deploy

default:
  image: debian:stable-slim
  tags:
    - docker

variables:
  # The runner will be able to pull your Zola theme when the strategy is
  # set to "recursive".
  GIT_SUBMODULE_STRATEGY: "recursive"

  # If you don't set a version here, your site will be built with the latest
  # version of Zola available in GitHub releases.
  # Use the semver (x.y.z) format to specify a version. For example: "0.17.2" or "0.18.0".
  ZOLA_VERSION:
    description: "The version of Zola used to build the site."
    value: ""

pages:
  stage: deploy
  script:
    - |
      apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
      if [ $ZOLA_VERSION ]; then
        zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
        if ! wget --quiet --spider $zola_url; then
          echo "A Zola release with the specified version could not be found.";
          exit 1;
        fi
      else
        github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
        zola_url=$(
          wget --output-document - $github_api_url |
          grep "browser_download_url.*linux-gnu.tar.gz" |
          cut --delimiter : --fields 2,3 |
          tr --delete "\" "
        )
      fi
      wget $zola_url
      tar -xzf *.tar.gz
      ./zola build

  artifacts:
    paths:
      # This is the directory whose contents will be deployed to the GitLab Pages
      # server.
      # GitLab Pages expects a directory with this name by default.
      - public

  rules:
    # This rule makes it so that your website is published and updated only when
    # you push to the default branch of your repository (e.g. "master" or "main").
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
- Hosted on Netlify. Sign up in Netlify. Then link your gitlab project to netlify. Deploy your project by adding the following `netlify.toml` file in your repository and removing the build command/publish directory in the admin interface.
```
[build]
# This assumes that the Zola site is in a docs folder. If it isn't, you don't need
# to have a `base` variable but you do need the `publish` and `command` variables.
publish = "/public"
command = "zola build"


[build.environment]
# Set the version name that you want to use and Netlify will automatically use it.
ZOLA_VERSION = "0.13.0"

# The magic for deploying previews of branches.
# We need to override the base url with whatever url Netlify assigns to our
# preview site.  We do this using the Netlify environment variable
# `$DEPLOY_PRIME_URL`.

[context.deploy-preview]
command = "zola build --base-url $DEPLOY_PRIME_URL"
```
- With this setup, your site should be automatically deployed on every commit on master.

# Screenshots

- Pipeline shows the GitLab workflow

![](pic/img1.png)

- Deploy the website on Netlify

![](pic/img2.png)

- Website - about me

![](pic/img3.png)

- Website - projects

![](pic/img4.png)

- Website - project detail page

![](pic/img5.png)