+++
title = "Sharon Ma"
description = "MS in Electrical & Computer Engineering at Duke University"
+++

**About Me**

- Education: 

    I graduated from Central University of Finance and Economics, China. Currently, I'm a MS of Electrical and Computer Engineering student at Duke University.<br><br>
    
- Programming Skills:

    Java, C++, Python, Swift, Rust, HTML, CSS, SQL<br><br>

- MBTI: intj