+++
title="Mobile App Development（ece564）"
date=2023-11-30

[taxonomies]
categories = ["Course"]
tags = ["course-project"]
[extra.author]
name = "Famous author"
+++

#### Restaurant Review App with App Clips
**Overview:**

This project aims to demonstrate the implementation of an App Clip using SwiftUI in the context of a restaurant review application for Duke's west campus. Users can scan a QRCode at a restaurant to quickly access the restaurant's reviews and ratings without having to download the entire application.

**Link:** [GitLab Link to Restaurant Review App](https://gitlab.oit.duke.edu/kits/ECE-564-01-F23/projects/bluedevil-bites.git)

**Features:**
- Main Application:
List of all restaurants on the west campus.
User registration and login.
Ability to rate and review a restaurant.
Search function to quickly find restaurants.
Integration with device maps to show restaurant locations.<br><br>

- App Clip:
Quick access to a particular restaurant's ratings and reviews.
Option to download the full application.
Secure and minimal data access for quick loading times.<br><br>

- QRCode Scanning:
Unique QRCode for each restaurant.
Direct access to that restaurant's App Clip.<br><br>

- Tutorial on Implementation: 
Detailed step-by-step guide on adding an App Clip to the app using a Playground file, drawing parallels with the ToDoList tutorial.