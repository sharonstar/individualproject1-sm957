+++
title="Data Analysis in Cloud Computing（ids721）"
date=2024-01-30

[taxonomies]
categories = ["Course"]
tags = ["course-project"]
[extra.author]
name = "Famous author"
+++

#### Mini-Project 1
Create a static site with Zola, a Rust static site generator, that will hold all of the portofolio work in this class.
- Requirements: Static site generated with Zola; Home page and portfolio project page templates; Styled with CSS; GitLab repo with source code

- Link to mini-project 1: [GitLab Link](https://gitlab.com/sharonstar/miniproject-1-sm957.git)<br><br>

#### Mini-Project 2
Create a simple AWS Lambda function that processes data. It achieved the calculator functionality of subtraction.
- Requirements: Rust Lambda Function using Cargo Lambda;
Process and transform sample data
- Link to mini-project 2: [GitLab Link](https://gitlab.com/sharonstar/miniproject2-sm957.git)<br><br>

#### Mini-Project 3
Create an S3 Bucket using CDK with AWS CodeWhisperer
- Requirements: Create S3 bucket using AWS CDK; Use CodeWhisperer to generate CDK code; Add bucket properties like versioning and encryption
- Link to mini-project 3: [GitLab Link](https://gitlab.com/sharonstar/miniproject3-sm957.git)<br><br>

#### Mini-Project 4
Containerize a Rust Actix Web Service
- Requirements: Containerize simple Rust Actix web app; Build Docker image; Run container locally
- Link to mini-project 4: [GitLab Link](https://gitlab.com/sharonstar/miniproject4-sm957.git)<br><br>

#### Mini-Project 5
Serverless Rust Microservice
- Requirements: Create a Rust AWS Lambda function; Implement a simple service; Connect to a database
- Link to mini-project 5: [GitLab Link](https://gitlab.com/sharonstar/miniproject5-sm957.git)<br><br>